import {
    Game
} from "phaser";
import Boot from './Scenes/Boot';
import Preload from './Scenes/Preload';
import MainMenu from './Scenes/MainMenu';
import Play from './Scenes/Play';
import {
    Squash,
    _1v1,
    BrickBreaker
} from './Scenes/GameModes';

import gameDefaults from './gameDefaults';

const app = new Game({
    title: gameDefaults.title,
    version: gameDefaults.version,
    type: Phaser.AUTO,
    width: gameDefaults.width,
    height: gameDefaults.height,
    parent: 'game',
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: {
            // debug: true
        }
    },
    input: {
        keyboard: false,
        gamepad: false,
        activePointers: 2
    },
    scene: [
        Boot,
        Preload,
        MainMenu,
        Play,
        Squash,
        _1v1,
        BrickBreaker
    ],
    resolution: window.devicePixelRatio
});

window.game = app;