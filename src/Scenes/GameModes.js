import {
    Scene
} from 'phaser';
import './Player.js';
import './Ball.js';
import gameDefaults from './../gameDefaults';

export class Squash extends Scene {
    constructor(config = 'squash') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        // console.log("Game has started!");
        const camera = this.cameras.main,
            midLineUpper = Math.floor((gameDefaults.height / 2) - (gameDefaults.lineWidth / 2)),
            midLineLower = Math.floor((gameDefaults.height / 2) + (gameDefaults.lineWidth / 2));

        camera.setBackgroundColor(gameDefaults.pallete.bgGreen);

        let _this = this;
        this.camera = camera;

        this.tableLines = this.add.image(0, 0, 'table');
        this.tableLines.setOrigin(0);

        this.score = 0;
        this.scoreText = this.add.text(0, 0, '0');
        this.scoreText.setPosition(camera.centerX + this.scoreText.width, camera.centerY - this.scoreText.height);

        // Player 2
        this.p_2 = this.add.player(camera.centerX, gameDefaults.height - 70, 'paddle', 'p_2');
        // console.log('Player 2', (window.p_2 = this.p_2, this.p_2));

        // Ball
        this.ball = this.add.ball(camera.centerX, camera.centerY, 'ball');
        this.ball.setVelocity(0, -gameDefaults.ballMaxVelocity);

        // When the ball goes out of bounds
        this.ball.body.world.on('worldbounds', function (body, up, down) {
            // console.log('Collided with bounds!');
            // if (this === gameObject) {
                if (down) {
                    this.scene.cameras.main.shake(140, 0.007);
                    alert('Your score: ' + this.scene.score);
                    this.scene.scene.start('MainMenu');
                } else {
                    this.snd_bounce.play();
                }
            // }
        }, this.ball);

        // console.log('Ball', (window.ball = this.ball, this.ball));

        this.physics.add.collider(this.ball, this.p_2);
        this.ball.body.world.on('collide', function () {
            this.scoreText.text = ++this.score;
            // console.log('Scored!');
        }, this);


        this.input.on('pointerup', pointer => {
            var x = pointer.position.x;
            var y = pointer.position.y;

            // If the pointer is being clicked in either the top or bottom part of the table, set the flag and target coords of respective paddle accordingly
            if (y > midLineLower && y > midLineLower) {
                // this.physics.moveToObject(this.p_2, pointer, 700);
                this.p_2.trackTarget = true;
                this.p_2.target.set(x, y);
            }
        });
        // this.restart();
    }

    update() {}
}

export class _1v1 extends Scene {
    constructor(config = '1v1') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        // console.log('1v1', 'has started!');
        this.scene.start('Play');
    }

    update() {}
}

export class BrickBreaker extends Scene {
    constructor(config = 'brick-breaker') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        // console.log("Game has started!");
        const camera = this.cameras.main,
            midLineUpper = Math.floor((gameDefaults.height / 2) - (gameDefaults.lineWidth / 2)),
            midLineLower = Math.floor((gameDefaults.height / 2) + (gameDefaults.lineWidth / 2));

        camera.setBackgroundColor(gameDefaults.pallete.bgGreen);

        let _this = this;
        this.camera = camera;

        this.tableLines = this.add.image(0, 0, 'table');
        this.tableLines.setOrigin(0);

        this.score = 0;
        this.scoreText = this.add.text(0, 0, '0');
        this.scoreText.setPosition(camera.centerX, camera.centerY - this.scoreText.height);

        this.blocks = this.physics.add.group({
            key: 'block',
            // classType: Phaser.Physics.Arcade.Image,
            repeat: gameDefaults.block.verticalCells * gameDefaults.block.horizontalCells - 1,
            immovable: true,
            // gridAlign: {
            //     cellHeight: gameDefaults.block.height,
            //     cellWidth: gameDefaults.block.width,
            //     height: gameDefaults.block.verticalCells,
            //     width: gameDefaults.block.horizontalCells,
            //     // x: gameDefaults.block.padding.left,
            //     // y: gameDefaults.block.padding.top,
            // },
            // setXY: {
            //     x: 0,
            //     y: 0,
            //     stepX: gameDefaults.block.width + gameDefaults.block.padding.left,
            //     stepY: gameDefaults.block.verticalCells + gameDefaults.block.padding.top,
            // }
        });
        Phaser.Actions.GridAlign(this.blocks.getChildren(), {
            cellHeight: gameDefaults.block.height,
            cellWidth: gameDefaults.block.width,
            height: gameDefaults.block.verticalCells,
            width: gameDefaults.block.horizontalCells,
            x: (gameDefaults.block.width / 1.6) + (0),
            y: (gameDefaults.block.height / 2)/*  + gameDefaults.block.padding.top */,
        });
        // for (let ix = 0, length = gameDefaults.block.horizontalCells * gameDefaults.block.verticalCells; ix < length; ix++) {
        //     this.blocks.add(this.physics.add.image(0,0,'block'));
        // }
        // console.log(this.blocks.getChildren());
        // Player 2
        this.p_2 = this.add.player(camera.centerX, gameDefaults.height - 70, 'paddle', 'p_2');
        // console.log('Player 2', (window.p_2 = this.p_2, this.p_2));

        // Ball
        this.ball = this.add.ball(camera.centerX, camera.centerY, 'ball');
        // this.ball.setActive(false);
        this.ball.setVelocity(0, -gameDefaults.ballMaxVelocity);

        // When the ball goes out of bounds
        this.ball.body.world.on('worldbounds', function ({
            gameObject
        }, blockedUp, blockedDown) {
            // console.log('Collided with bounds!');
            if (this === gameObject) {
                if (blockedDown) {
                    this.scene.cameras.main.shake(140, 0.007);
                    alert('Your score: ' + this.scene.score);
                    this.scene.scene.start('MainMenu');
                } else {
                    this.snd_bounce.play();
                }
            }
        }, this.ball);

        // console.log('Ball', (window.ball = this.ball, this.ball));
        // this.physics.add.overlap()
        this.physics.add.collider(this.ball, this.blocks.getChildren(), (ball, block) => {
            // 
            // this.blocks.killAndHide(block);
            block.setActive(false);
            block.setVisible(false);
            ball.body.velocity.y = gameDefaults.ballMaxVelocity;
        });
        this.physics.add.collider(this.ball, this.p_2);
        this.ball.body.world.on('collide', function () {
            this.scoreText.text = ++this.score;
            // console.log('Scored!');
        }, this);


        this.input.on('pointerup', pointer => {
            var x = pointer.position.x;
            var y = pointer.position.y;

            // If the pointer is being clicked in either the top or bottom part of the table, set the flag and target coords of respective paddle accordingly
            if (y > midLineLower && y > midLineLower) {
                // this.physics.moveToObject(this.p_2, pointer, 700);
                this.p_2.trackTarget = true;
                this.p_2.target.set(x, y);
            }
        });
    }

    update() {}
}