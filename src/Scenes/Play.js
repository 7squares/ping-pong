import {
    Scene
} from 'phaser';
import './Player.js';
import './Ball.js';
import gameDefaults from './../gameDefaults';

class Play extends Scene {
    constructor(config = 'Play') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        // console.log("Game has started!");
        const camera = this.cameras.main,
            midLineUpper = Math.floor((gameDefaults.height / 2) - (gameDefaults.lineWidth / 2)),
            midLineLower = Math.floor((gameDefaults.height / 2) + (gameDefaults.lineWidth / 2));

        camera.setBackgroundColor(gameDefaults.pallete.bgGreen);

        let _this = this;
        this.camera = camera;

        this.tableLines = this.add.image(0, 0, 'table');
        this.tableLines.setOrigin(0);

        // 1 is player 1, 2 is player 2
        this.whoscored = Math.round(1 + (Math.random() * (2 - 1)));
        this.score_p1 = 0;
        this.score_p2 = 0;

        // Player 1
        this.p_1 = this.add.player(camera.centerX, 70, 'paddle', 'p_1');
        // Rotate the player by 180 degrees
        this.p_1.setRotation(Math.PI);
        // Move the body to the lower half so that it has
        this.p_1.body.setOffset(0, this.p_1.body.halfWidth);
        // console.log('Player 1', (window.p_1 = this.p_1, this.p_1));

        // Player 2
        this.p_2 = this.add.player(camera.centerX, gameDefaults.height - 70, 'paddle', 'p_2');
        // console.log('Player 2', (window.p_2 = this.p_2, this.p_2));

        // Ball
        this.ball = this.add.ball(camera.centerX, camera.centerY, 'ball');

        // When the ball goes out of bounds
        this.ball.body.world.on('worldbounds', function ({
            gameObject
        }) {
            // console.log('Collided with bounds!');
            // if (this === gameObject) {
            if (this.y >= gameDefaults.height / 2) {
                this.scene.whoscored = 1;
                this.scene.score_p1++;
                // console.log(this.scene.score_p1);
            } else {
                this.scene.whoscored = 2;
                this.scene.score_p2++;
                // console.log(this.scene.score_p2);
            }
            this.scene.cameras.main.shake(140, 0.007);
            this.scene.restart();
            // }
        }, this.ball);

        // console.log('Ball', (window.ball = this.ball, this.ball));

        this.physics.add.collider(this.ball, [this.p_1, this.p_2]);
        this.ball.on('collide', function () {
            // console.log('Collision!');
        });


        this.input.on('pointerup', pointer => {
            var x = pointer.position.x;
            var y = pointer.position.y;

            // If the pointer is being clicked in either the top or bottom part of the table, set the flag and target coords of respective paddle accordingly
            if (y > midLineLower && y > midLineLower) {
                // this.physics.moveToObject(this.p_2, pointer, 700);
                this.p_2.trackTarget = true;
                this.p_2.target.set(x, y);
            } else if (y < midLineUpper && y < midLineUpper) {
                // this.physics.moveToObject(this.p_1, pointer, 700);
                this.p_1.trackTarget = true;
                this.p_1.target.set(x, y);
            }
        });
        this.restart();
    }

    update(dt) {}
    restart() {
        this.p_1.setPosition(70, 70, 'paddle');
        this.p_2.setPosition(this.camera.displayWidth - 70, gameDefaults.height - 70, 'paddle');
        this.ball.setPosition(this.camera.centerX, this.camera.centerY, 'ball');

        if (this.score_p1 == 2) {
            // Player 1 has won
            alert('Player 1 has won!');
            this.scene.start('MainMenu');
        } else if (this.score_p2 == 2) {
            // Player 2 has won
            alert('Player 2 has won!');
            this.scene.start('MainMenu');
        } else {
            // console.log('Hey', this.whoscored);
            // Player 1 just scored. Move the ball towards other player when game starts
            if (this.whoscored == 1) {
                // Position ball near other paddle
                this.ball.setPosition(this.p_2.body.center.x, this.p_2.body.center.y - (this.p_2.body.halfHeight + this.ball.body.height) - 7);
                // Make ball move towards player
                // this.physics.moveToObject(this.ball, this.p_1, this.ball.body.maxVelocity.x);
                this.physics.moveTo(this.ball, this.camera.centerX, this.p_1.y, this.ball.body.maxVelocity.x);
            }
            // Player 2 just scored. Move the ball towards other player when game starts
            else if (this.whoscored == 2) {
                // Position ball near other paddle
                this.ball.setPosition(this.p_1.body.center.x, this.p_1.body.center.y + this.p_1.body.halfHeight + this.ball.body.height + 7);
                // Make ball move towards player
                // this.physics.moveToObject(this.ball, this.p_2, this.ball.body.maxVelocity.x);
                this.physics.moveTo(this.ball, this.camera.centerX, this.p_2.y, this.ball.body.maxVelocity.x);
            }
        }
    }
}

export default Play;