import { Scene } from 'phaser';

class Boot extends Scene {
    constructor(config = 'Boot') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        this.scene.start('Preload');
    }

    update() {}
}

export default Boot;