import {
    Physics,
    
} from 'phaser';
import gameDefaults from './../gameDefaults';

class Player extends Physics.Arcade.Image {
    constructor(scene, x, y, key) {
        super(scene, x, y, key);

    }
}

Phaser.GameObjects.GameObjectFactory.register('player', function (x, y, key) {
    let sprite = new Physics.Arcade.Image(this.scene, x, y, key);

    const factory = this.scene.physics.add;

    factory.sys.displayList.add(sprite);
    factory.sys.updateList.add(sprite);

    factory.world.enableBody(sprite, Phaser.Physics.Arcade.DYNAMIC_BODY);

    // This is so that the player can't be moved during collisions
    sprite.setImmovable(true);

    // Allow player to collide with the wall
    sprite.setCollideWorldBounds(true);

    // Flag for player identification
    sprite.isPlayer = true;

    // Set the objects friction so that their is none
    sprite.setFriction(0, 0);

    // Make the sprite/image bigger
    sprite.setScale(gameDefaults.scale);

    // Convert the image's body to be a circle
    sprite.setCircle(Math.floor(sprite.width / 2));

    // The pointer's position
    sprite.target = new Phaser.Math.Vector2();

    // Used for finding the angle between the two objects
    sprite.targetDistLength = new Phaser.Math.Vector2();

    // If true we move towards the target vector
    sprite.trackTarget = false;

    // Maximum velocity
    sprite.setMaxVelocity(gameDefaults.playerMaxVelocity);

    // The distance at which we start to slow down
    sprite.slowRadius = 81;

    // Update function
    sprite.preUpdate = function () {
        // If we should track
        if (this.trackTarget) {
            // Get the distance to the target from the player
            var distance = this.body.center.distance(this.target);
            // Set the velocity so that player is moving towards the target at set speed
            this.body.velocity.setToPolar(
                // Angle to target
                this.targetDistLength.copy(this.target).subtract(this).angle(),
                // Speed
                gameDefaults.playerMaxVelocity
            );

            // if (distance < this.slowRadius) {
            //     this.body.velocity.scale(distance / this.slowRadius);
            //     // this.trackTarget = false;
            // }
            if (distance < this.slowRadius  * .5 ) {
                this.setVelocity(0);
                this.setPosition(this.target.x, this.target.y);
                this.trackTarget = false;
            }
        }
    };

    return sprite;
});