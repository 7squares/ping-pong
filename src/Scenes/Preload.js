import {
    Scene
} from 'phaser';
import gameDefaults from './../gameDefaults';

class Preload extends Scene {
    constructor(config = 'Preload') {
        super(config);
    }

    init() {}

    preload() {
        // Either load all assets

        this.load.baseURL = location.href;
        // Paddle graphic
        this.load.image('paddle', 'img/paddle.png');
        this.load.image('logo', 'img/logo.png');
        this.load.image('squash', 'img/squash.png');
        this.load.image('1v1', 'img/1v1.png');
        this.load.image('brick-breaker', 'img/brick-breaker.png');
        this.load.audio('bounce', 'audio/bounce.ogg');
        this.load.bitmapFont('mx', 'img/m5x7_0.png', 'img/m5x7.xml');
    }

    create() {
        // Generate assets

        // Table
        const table = this.add.graphics();
        const lineWidth = gameDefaults.lineWidth;

        table.lineStyle(lineWidth, gameDefaults.pallete.lineColor);

        // Here we draw the table's boundary lines

        // Table dimensions
        const w = gameDefaults.width;
        const h = gameDefaults.height;

        // Start the path drawing
        table.beginPath();
        // Top
        table.moveTo(0, 0);
        table.lineTo(w, 0);
        // Right
        table.moveTo(w, 0);
        table.lineTo(w, h);
        // Bottom
        table.moveTo(w, h);
        table.lineTo(0, h);
        // Left
        table.moveTo(0, h);
        table.lineTo(0, 0);
        // Middle
        table.moveTo(w / 2, 0);
        table.lineTo(w / 2, h);
        table.strokePath();
        // Done!
        table.closePath();


        // Net shadow, but I don't think it will be seen as such
        table.fillStyle(gameDefaults.pallete.shadeGreen);

        const pixel = lineWidth,
            startY = Math.floor((h / 2) + (pixel / 2));

        for (let ix = 0; ix < w; ix += pixel) {
            for (let kx = 0, length2 = 3; kx < (pixel * length2); kx += pixel) {
                // 
                let ixOdd = true;

                // Is odd
                if ((ix / pixel) % 2) {
                    // 
                    ixOdd = true
                }
                // Is even
                else {
                    // 
                    ixOdd = false;
                }

                // If it's an odd number
                if ((kx / pixel) % 2) {
                    // 
                    if (!ixOdd) {
                        // Draw only if ix and kx are both odd
                        // table.fillStyle(0xff00ff);
                        table.fillRect(ix, startY + kx, pixel, pixel);
                    }
                }
                // If it's an even number
                else {
                    // Draw only on even numbered kx indexes
                    if (ixOdd) {
                        // Do stuff
                        // table.fillStyle(0xffff00);
                        table.fillRect(ix, startY + kx, pixel, pixel);
                    }
                }

                // Draw the last line
                if (ix + pixel > w) {
                    table.fillRect(0, startY + (pixel * length2), w, pixel);
                }
            }
        }

        // Now we draw the net in the middle of the screen
        table.lineStyle(pixel, gameDefaults.pallete.middleColor);

        table.beginPath();
        table.moveTo(0, gameDefaults.height / 2);
        table.lineTo(gameDefaults.width, gameDefaults.height / 2);
        table.strokePath();
        table.closePath();

        // Turn this into a texture
        table.generateTexture('table').setVisible(false);

        // Ball
        const ball = this.add.graphics();
        ball.fillStyle(gameDefaults.pallete.lineColor);
        ball.fillRect(1, 0, 1, 1);
        ball.fillRect(2, 0, 1, 1);
        ball.fillRect(0, 1, 1, 1);
        ball.fillRect(1, 1, 1, 1);
        ball.fillRect(3, 1, 1, 1);
        ball.fillRect(0, 2, 1, 1);
        ball.fillRect(1, 2, 1, 1);
        ball.fillRect(2, 2, 1, 1);
        ball.fillRect(3, 2, 1, 1);
        ball.fillRect(1, 3, 1, 1);
        ball.fillRect(2, 3, 1, 1);
        ball.fillStyle(gameDefaults.pallete.white);
        ball.fillRect(2, 1, 1, 1);
        ball.generateTexture('ball', 4, 4).setVisible(false);

        // Create the block texture

        const block = this.add.graphics();

        block.fillStyle(gameDefaults.pallete.block);
        block.fillRect(gameDefaults.block.padding.left,  gameDefaults.block.padding.top, gameDefaults.block.width, gameDefaults.block.height);
        block.generateTexture('block', gameDefaults.block.width + gameDefaults.block.padding.left, gameDefaults.block.height + gameDefaults.block.padding.top);

        this.scene.start('MainMenu');
    }

    update() {}
}

export default Preload;