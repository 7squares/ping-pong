import { Scene } from 'phaser';
import gameDefaults from './../gameDefaults';

class MainMenu extends Scene {
    constructor(config = 'MainMenu') {
        super(config);
    }

    init() {}

    preload() {}

    create() {
        // Get the main camera
        this.cameras.main.setBackgroundColor(gameDefaults.pallete.shadeGreen);
        // Place the logo onto the screen
        const logo = this.add.image(this.cameras.main.centerX, Math.floor(this.cameras.main.centerY / 2), 'logo').setScale(2.5).setTintFill(gameDefaults.pallete.lineColor);
        
        // Place menu items
        let menuItems = ['squash', '1v1', 'brick-breaker'];

        for (let ix = 0, length = menuItems.length; ix < length; ix++) {
            // 
            let menuItem = this.add.image(this.cameras.main.centerX, logo.height + this.cameras.main.centerY + 70 * ix, menuItems[ix]).setScale(1.7);

            const sceneKey = menuItems[ix].replace(/[-]/g, '_');

            menuItem.setInteractive({
                useHandCursor: true
            });
            menuItem.on('pointerdown', () => {
                // console.log('pointer down')
                this[`start${sceneKey}`]();
            })
        };

        // this.input.on('pointerup', () => {
            // this.scene.start('Play');
        // });
    }

    startsquash() {
        alert("INSTRUCTIONS: return the ball without it touching any of the edges IN YOUR HALF");
        this.scene.start('squash');
    }
    start1v1() {
        alert("INSTRUCTIONS: Table Tennis for two!!");
        this.scene.start('1v1');
    }
    startbrick_breaker() {
        alert("INSTRUCTIONS: It's brick breaker... the ping pong edition. Though this time if the ball hits the BOTTOM EDGE it's game over");
        this.scene.start('brick-breaker');
    }

    update() {}
}

export default MainMenu;