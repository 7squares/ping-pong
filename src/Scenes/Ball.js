import {
    Physics,

} from 'phaser';
import gameDefaults from './../gameDefaults';

Phaser.GameObjects.GameObjectFactory.register('ball', function (x, y, key) {
    let sprite = new Physics.Arcade.Image(this.scene, x, y, key);

    const factory = this.scene.physics.add;

    factory.sys.displayList.add(sprite);
    factory.sys.updateList.add(sprite);

    factory.world.enableBody(sprite, Phaser.Physics.Arcade.DYNAMIC_BODY);

    // The ball bounce sound
    sprite.snd_bounce = this.scene.sound.add('bounce');

    // The maximum velocity of ball. Also used as the main speed at which ball moves in game
    sprite.setMaxVelocity(gameDefaults.ballMaxVelocity, gameDefaults.ballMaxVelocity);

    // Ball is too small, thus it's scaled up
    sprite.setScale(gameDefaults.scale);

    // Body type must match sprite
    sprite.setCircle(Math.floor(sprite.width / 2));
    
    // The ball should emit a 'collide' event
    sprite.body.onCollide = true;
    // When the ball collides with the paddle
    sprite.body.world.on('collide', function (spr1, spr2) {
        /* 
            Tested the game on some of my classmates and they tried to drag the paddle around.
            
            Should communicate that player needs to only touch the screen and not drag. Should also make sure input listener is onPointerUp in case user does drag.
            
            Not certain if dragging, being more computationally expensive than a simple touch, would be a good idea when there are two players trying to quickly move their paddle around in a frantic local pvp game.
            */
        var ball = !spr1.isPlayer ? spr1 : spr2;
        var player = ball === spr1 ? spr2 : spr1;

        const bX = ball.body.center.x,
            bY = ball.body.center.y,
            pX = player.body.center.x,
            pY = player.body.center.y;

        // Bounce ball based on the x prop
        if (bX > pX) {
            //
            this.velocity.x = this.maxVelocity.x;
        } else if (bX < pX) {
            this.velocity.x = -this.maxVelocity.x;
        }

        // Bounce ball based on the y prop
        if (bY > pY) {
            this.velocity.y = this.maxVelocity.y;
        } else if (bY < pY) {
            this.velocity.y = -this.maxVelocity.y;
        }

        this.gameObject.snd_bounce.play();
        const angle = this.velocity.angle();
        const angleOffset = 0.122173;
        this.velocity.setToPolar(angle + (-angleOffset + (Math.random() * (angleOffset - -angleOffset))), this.maxVelocity.x);
    }, sprite.body);

    // The ball should collide with the world bounds and emit an event
    sprite.setCollideWorldBounds(true);
    sprite.body.onWorldBounds = true;

    // Also, collisions should result in a bounce
    sprite.setBounce(1);

    sprite.preUpdate = () => {};

    return sprite;
});