const gameDefaults = {
    title: 'Ping Pong',
    version: '<%VERSION%>',
    width: window.innerWidth,
    height: window.innerHeight,
    scale: 4.3,
    ballMaxVelocity: 430,
    playerMaxVelocity: 700,
    block: {
        horizontalCells: 5,
        verticalCells: 5,
        padding: {
            top: 2,
            bottom: 2,
            right: 2,
            left: 2
        },
        width: 0,
        height: 0
    },
    pallete: {
        bgGreen: Phaser.Display.Color.HexStringToColor('#ab963a').color,
        shadeGreen: Phaser.Display.Color.HexStringToColor('#867e49').color,
        lineColor: Phaser.Display.Color.HexStringToColor('#eed4a3').color,
        middleColor: Phaser.Display.Color.HexStringToColor('#462b13').color,
        white: Phaser.Display.Color.HexStringToColor('#ffffff').color,
        block: Phaser.Display.Color.HexStringToColor('#f4b03c').color,
    },
    lineWidth: 7
};

// If we are in portrait orientation
if (gameDefaults.width > gameDefaults.height) {
    gameDefaults.width = Math.floor(window.innerWidth * .43);
}

// Calculate block height and width
gameDefaults.block.width = Math.floor(gameDefaults.width / gameDefaults.block.horizontalCells) - (gameDefaults.block.padding.right + gameDefaults.block.padding.left);
gameDefaults.block.height = Math.floor((gameDefaults.height / 3) / gameDefaults.block.verticalCells) - (gameDefaults.block.padding.bottom + gameDefaults.block.padding.top);
console.log(gameDefaults.block)
export default gameDefaults;