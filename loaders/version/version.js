// // This will run after the version has been bumped
// const fs = require('fs');
// const path = require('path');

// // The main js bundle
// const file = fs.readFileSync(path.join(__dirname, 'public/js/main.js'), 'utf8');

// const data = file.replace(/<%VERSION%>/g, version);

// fs.writeFileSync(path.join(__dirname, 'public/js/main.js'), data, 'utf8');
const version = require('./../../package.json').version;

module.exports = function (src) {
    // The current semver version number

    // The new data
    const data = src.replace(/<%VERSION%>/g, version);

    return data;
};